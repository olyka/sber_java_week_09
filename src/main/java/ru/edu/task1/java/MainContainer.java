package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class MainContainer {

    @Autowired
    private ComponentA componentA;

    @Autowired
    private ComponentB componentB;

    public boolean isValid() {
        return componentA != null && componentB != null && componentA.isValid() && componentB.isValid();
    }
}
