package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
@PropertySource("classpath:task_01.properties")
public class AppJava {

    @Value("${componentB.default.string}")
    String stringForComponentB;

    public static MainContainer run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }

    @Bean(initMethod = "init")
    public ComponentC componentC() {
        ComponentC componentC = new ComponentC();
        return componentC;
    }

    @Bean
    public ComponentA componentA() {
        ComponentA componentA = new ComponentA(new ComponentC());
        return componentA;
    }

    @Bean
    public ComponentB componentB() {
        ComponentB componentB = new ComponentB(stringForComponentB);
        return componentB;
    }

    @Bean
    public MainContainer mainContainer() {
        MainContainer mainContainer = new MainContainer();
        return mainContainer;
    }

}
