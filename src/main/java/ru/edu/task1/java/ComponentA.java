package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * ReadOnly
 */
public class ComponentA {

    @Autowired
    private ComponentC componentC;

    public ComponentA(ComponentC c) {
        componentC = c;
    }

    public boolean isValid(){
        return componentC != null && componentC.isValid();
    }
}
