package ru.edu.task3.java;

import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private DependencyObject dependency;


    public MainContainer(DependencyObject dependencyBean) {
        dependency = dependencyBean;
    }

    public String getValue() {
        return dependency.getValue();
    }
}
