package ru.edu.task5.java;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import ru.edu.task5.common.InterfaceToRemove;
import ru.edu.task5.common.InterfaceToWrap;

public class MyPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof InterfaceToWrap) {
            return (InterfaceToWrap) () -> "wrapped " + ((InterfaceToWrap) bean).getValue();
        }

        if (bean instanceof InterfaceToRemove) {
            return "";
        }

        return bean;

    }
}
