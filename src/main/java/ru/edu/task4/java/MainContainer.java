package ru.edu.task4.java;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private final SomeInterface someInterface;

    public MainContainer(@Qualifier("cacheService") SomeInterface someInterfaceBean) {
        someInterface = someInterfaceBean;
    }

    public SomeInterface getSomeInterface() {
        return someInterface;
    }
}
