package ru.edu.task4.java;

import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component("realService")
public class RealService implements SomeInterface {
    @Override
    public String getName() {
        return "RealService";
    }
}
