package ru.edu.task4.java;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component("cacheService")
public class CacheService implements SomeInterface {

    private final SomeInterface delegate;

    public CacheService(@Qualifier("realService") SomeInterface delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getName() {
        return "cacheService of " + delegate.getName();
    }
}
