package ru.edu.task2.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private final TimeKeeper keeper;

    private final Child child;

    @Autowired
    public MainContainer(TimeKeeper keeperBean, Child childBean) {
        keeper = keeperBean;
        child = childBean;
    }

    public boolean isValid() {
        if(keeper == null || child == null){
            throw new RuntimeException("Есть пустая зависимость");
        }
        if(keeper.equals(child.getTimeKeeper())){
            throw new RuntimeException("Зависимости не должны совпадать");
        }
        return true;
    }
}
