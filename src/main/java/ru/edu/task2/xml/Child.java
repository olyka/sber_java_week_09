package ru.edu.task2.xml;

/**
 * ReadOnly
 */
public class Child {

    /**
     * Можно добавить сеттер или конструктор
     * или Autowired
     */
    private TimeKeeper timeKeeper;

    public Child(TimeKeeper timeKeeper) {
        this.timeKeeper = timeKeeper;
    }

    public TimeKeeper getTimeKeeper() {
        return timeKeeper;
    }
}
